# c-security
A JavaScript project

For this project I have used the following tools:

nightwatch: https://nightwatchjs.org/
faker: https://www.npmjs.com/package/faker

Tests can be found in test/e2e/specs/

The layout of my tests consists the following:
    
    Company, email address , first names, last name (these have been pre-defined using faker) can be found in page-objects.
    
    One product has been included in the test.
    
    Once the input fields have been populated and the product has been selected the submit button will be clicked and test will end. 
