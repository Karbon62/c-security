const { mockedUser } = require('../../setup');

const fieldValidatorCommand = {
  fieldinput() {
    return this.waitForElementVisible('[id="id_company_name"]', 60000)
      .setValue('Bug Hunting Ltd', 60000)
      .waitForElementVisible('[id="id_admin_email"]', 60000)
      .setValue('input[type=email]', mockedUser.email)
      .waitForElementVisible('[id="id_admin_first_name"]', 6000)
      .setValue('input[type=name]', mockedUser.first_name)
      .waitForElementVisible('[id="id_admin_last_name"]', 60000)
      .setValue('input[type=last_name]', mockedUser.last_name);
  },
};
module.exports = {
  field_validator(browser) {
    browser
      .url('https://hudson-poutine-83447.herokuapp.com/')
      .end();
  },
};
