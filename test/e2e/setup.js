const faker = require('faker');

const createUserData = () => ({
  email: `test_${faker.internet.email()}`,
  company: 'company_name',
  first_name: 'first_name',
  last_name: 'last_name',
});

const mockedUser = createUserData();
module.exports = {
  mockedUser,
  createUserData,
};
