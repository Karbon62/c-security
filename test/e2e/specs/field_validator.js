// Company, Name, details
module.exports = {
  'fieldTypeInput in': (browser) => {
    const pageObj = browser.page.auth.fieldValidatorCommand();
    pageObj.navigate().fieldinput();
  },

  // Product Type
  'Step two: Select Product Type': function (browser) {
    browser
      .url(`${browser.launchUrl}`)
      .waitForElementVisible('[id="id_product_0"]', 8000)
      .click('[id="id_product_0"]');
  },

  // Submit
  'Step three: Submit detail': function (browser) {
    browser
      .url(`${browser.launchUrl}`)
      .waitForElementVisible('body')
      .waitForElementVisible('[value="Submit"]', 6000)
      .click('[value="Submit"]')
      .end();
  },
};
